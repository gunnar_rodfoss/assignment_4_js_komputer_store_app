
const GetLoanButton = document.getElementById("GetLoanButton");
const SalaryElement = document.getElementById("PayEL");
const balanceElement = document.getElementById("BalanceElement");
const bankButton = document.getElementById("BankButton");
const repayLoanButton = document.getElementById("RepayLoanButton");
const WorkButton = document.getElementById("WorkButton");
const LoanValueAmount = document.getElementById("LoanValueAmount");
const ComputerInfoText = document.getElementById("ComputerInfoText");
const ComputerNameHeading = document.getElementById("ComputerNameHeading");
const computerPrice = document.getElementById("computerPrice");

const LaptopFeatures = document.getElementById("LaptopFeatures");

const BuyNowButton = document.getElementById("BuyNowButton");

const LaptopList = document.getElementById("LaptopList");
const LaptopImg = document.getElementById("LaptopImg");
const LaptopSelector = document.getElementById("LaptopSelector");

let LoanValueDisplay = Array.from( document.getElementsByClassName("LoanValue") );

let loanAmount=0;
let salery = 0;
let bankBalace = 0;
let repayLoan = false;
/////////Function to update the values that is supose to be displayed////////////
function updateDisplayValues(){
    SalaryElement.innerText = `${salery}kr`
    balanceElement.innerText = `${bankBalace}kr`
    LoanValueAmount.innerText = ` ${loanAmount} kr`;
}
//////////WORKBUTTON LOGIC ///////////////////////////////
WorkButton.addEventListener('click', function(){
    if(loanAmount > 0 && loanAmount-(100*0.1) > 0 ){
        salery+=100*0.9;
        loanAmount-=100*0.1;
        updateDisplayValues()
    }
    else if( loanAmount-(100*0.1) < 0 && loanAmount !== 0){
        salery += (100*0.1)-loanAmount;
        loanAmount=0;
        updateDisplayValues()
        
    }
    else{
        salery+=100;
        loanAmount=0;
        updateDisplayValues()
    }
})
/////////Bank button //////////////////////////
bankButton.addEventListener('click', ()=>{
    if(!repayLoan){
        bankBalace += salery;
        salery = 0;
        updateDisplayValues();
    }
    else{
        payBackLoan()
        salery = 0;
        updateDisplayValues(); 
    }
})
/////////////Get a lone button///////////
GetLoanButton.addEventListener('click', function(){

    getLoan()
    updateDisplayValues()
    displayLoan();
})
                            function getLoan(){
                                if(bankBalace && loanAmount === 0){
                                    
                                    loanAmount = Math.abs( Number(window.prompt("Type a Loan amount", "")) );
                                    //check if the user types inn characters insted of valid numbers
                                    if (! /^[0-9.,]+$/.test(loanAmount)) {
                                        alert("Not a valid number")
                                        loanAmount = 0;
                                        getLoan()
                                      }
                                    else{
                                    if(loanAmount <= bankBalace * 2){
                                        repayLoanButton.style.visibility = "visible";
                                        bankBalace+=loanAmount;
                                        updateDisplayValues()
                                        return
                                    }
                                    else{
                                        alert("To high, not a noff balance")
                                        loanAmount = 0;
                                        getLoan()
                                    }
                                }
                                }
                            }


                            function displayLoan(){
                                LoanValueAmount.innerText = ` ${loanAmount}kr`;
                                    if(loanAmount){
                                        LoanValueDisplay.forEach(i => i.style.visibility = 'visible');
                                        //LoanValueDisplay.style.visibility = 'visible'
                                    }
                                    else{

                                        LoanValueDisplay.forEach(i => i.style.visibility = 'hidden');
                                    // LoanValueDisplay.style.visibility = 'hidden'
                                    }
                                }



///////////////////////Repay a lone button///////////////////
repayLoanButton.addEventListener('click',()=>{
    repayLoan = true;
})
            function payBackLoan(){
                if(loanAmount <= salery){
                    bankBalace += salery-loanAmount;
                    loanAmount = 0;
                    updateDisplayValues();
                    repayLoan = false;
                }
                else{
                    loanAmount  = loanAmount-salery;
                    updateDisplayValues();
                }
            }

//////////////Buy Now Button ////////////////////////////////
BuyNowButton.addEventListener('click',()=>{
    let price = parseInt( computerPrice.innerText.split('Kr') )
    if(price > bankBalace){
        alert("you dont have anoff money");
    }
    else{
        bankBalace -= price;
        alert("you are now a owner of a new laptop");
        updateDisplayValues();
    }
})









//loops true the data from the fetch respons
function getRespons(el){
    LaptopSelector.innerHTML = "";
    el.forEach(e =>{
        LaptopSelector.insertAdjacentHTML("beforeend",`<option value=${e.id}>${e.title}</option>`);
    })

   LaptopSelector.addEventListener('click',(e)=>{
        
        selectedItem = LaptopSelector.options[LaptopSelector.selectedIndex].value;
        let img = "https://noroff-komputer-store-api.herokuapp.com/" + el[LaptopSelector.selectedIndex].image;
        let description = ( el[LaptopSelector.selectedIndex].description);
        let heading = ( el[LaptopSelector.selectedIndex].title);
        let price = el[LaptopSelector.selectedIndex].price;
        
        LaptopFeatures.innerHTML = "";
        el[LaptopSelector.selectedIndex].specs.map(e =>{
            console.log(e)
            LaptopFeatures.innerHTML += `<li>${e}</li>`;
            
        })

        LaptopImg.src = img;
        ComputerInfoText.innerText  = description;
        ComputerNameHeading.innerText = heading;
        computerPrice.innerText = price +" "+ "Kr";
    })
    
}



async function  getLaptops(){
    const laptops = await fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => getRespons(data));
}
getLaptops()
